import javax.swing.*;
import java.awt.event.ActionEvent;
import java.awt.event.ActionListener;

public class FoodGUI {
    private JPanel root;
    private JButton TempuraButton;
    private JButton RamenButton;
    private JButton UdonButton;
    private JButton KirinBeerButton;
    private JButton AsahiBeerButton;
    private JButton SapporoBeerButton;
    private JTextArea textArea1;
    private JLabel total;
    private JButton CancelButton;
    private JButton CheckoutButton;

    int sum=0;


    void order(String food,int price) {
        int confirmation2 = JOptionPane.showConfirmDialog(null,
                "Would you like to order " + food + "?",
                "Order Confirmation",
                JOptionPane.YES_NO_OPTION);

        if (confirmation2 == 0) {
            int confirmation3 = JOptionPane.showConfirmDialog(null,
                    "Would you like a large one ?",
                    "Order Confirmation",
                    JOptionPane.YES_NO_OPTION);

            if (confirmation3 == 1) {
                JOptionPane.showMessageDialog(null, "Order for " + food + " received");

                String currentText = textArea1.getText();
                textArea1.setText(currentText + food + "      " + price + " yen" + "\n");

                sum += price;
                total.setText("Total    " + sum + "yen");
            }

            if (confirmation3 == 0) {
                JOptionPane.showMessageDialog(null, "Order for " + food + "(Large one) received");
                int price2 = price+100;
                String currentText = textArea1.getText();
                textArea1.setText(currentText + food + "(Large one)      " + price2 + " yen" + "\n");
                sum += price2;
                total.setText("Total    " + sum + "yen");
            }
        }
    }


    public FoodGUI() {
        TempuraButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                order("Tempura",1900);
            }
        });


        RamenButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                order("Ramen",1300);
            }
        });


        UdonButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                order("Udon", 1100);
            }
        });


        KirinBeerButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                order("Kirin Beer", 800);
            }
        });


        AsahiBeerButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                order("Asahi Beer", 600);
            }
        });


        SapporoBeerButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                order("Sapporo Beer", 400);
            }
        });


        CheckoutButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                int confirmation = JOptionPane.showConfirmDialog(null, "Would you like to check out ?",
                        "Order Confirmation", JOptionPane.YES_NO_OPTION);

                if (confirmation == 0) {
                    JOptionPane.showMessageDialog(null, "Thank you. The total price is "+ sum + " yen.");
                    textArea1.setText("");
                    total.setText("");
                    sum = 0;

                }
            }
        });


        CancelButton.addActionListener(new ActionListener() {
            @Override
            public void actionPerformed(ActionEvent e) {
                int confirmation = JOptionPane.showConfirmDialog(null, "Would you like to cancel ?",
                        "Order Confirmation", JOptionPane.YES_NO_OPTION);

                if (confirmation == 0) {
                    textArea1.setText("");
                    total.setText("");
                    sum = 0;
                }

            }
        });

    }


    public static void main(String[] args) {
        JFrame frame = new JFrame("FoodGUI");
        frame.setContentPane(new FoodGUI().root);
        frame.setDefaultCloseOperation(JFrame.EXIT_ON_CLOSE);
        frame.pack();
        frame.setVisible(true);
    }

}
